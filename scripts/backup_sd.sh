#!/bin/bash
#

log(){
	printf " :: $1 \n"
}

err(){
	log "ERROR: $1"
	exit 1
}

usage(){
	log "Usage: sudo `basename $0` </dev/name> <filename>"
}

trapi(){
	printf "\n"
	rm -rf ${IMG_FILE}
	sync
	err "Trapping, exit..."
}

main(){
	set -e
	trap "trapi" INT ERR

	COMP="${COMP:-xz}" # xz gz

	case "${COMP}" in
		zst)
			CAPP="zstd" ;;
		xz)
			CAPP="xz" ;;
		gz)
			CAPP="gzip" ;;
		*)
			COMP="xz"
			CAPP="xz" ;;
	esac

	DEV="$1"
	DESTDIR="${DESTDIR:-${PWD}}"
	IMG_FILE="${DESTDIR}/${2}.img.${COMP}"

	[[ "$UID" = 0 ]] || err "Run this with sudo!"

	if [ "$1" = "" ]; then
		usage
		err "Not set device"
	elif [ "$2" = "" ]; then
		usage
		err "Not set filename"
	fi

	log "Create '`basename ${IMG_FILE}`' from '${DEV}'"
	
	dd if=${DEV} conv=sync,noerror bs=1M status=progress | ${CAPP} -9 -c > ${IMG_FILE}
	chown ${USER}:${USER} ${IMG_FILE}

	sync
	log "Done."
}

main "$@"
